from faan.core.X.xflask import Controller, Route, request, capp
from faan.core.model.pub.application import Application
from faan.core.model.pub.zone import Zone
from faan.core.model.meta import Session
from faan.core.model.adv.campaign import Campaign
from faan.core.model.security.account import Account
from faan.core.model.adv.media import Media
from decimal import Decimal
from sqlalchemy.dialects.postgresql.base import Any
from flask import json, redirect, jsonify
from faan.core.model.pub.statistics import PubStat
from faan.core.model.adv.statistics import AdvStat
from sqlalchemy.sql.expression import desc, or_, not_, and_
from faan.x.util.func import try_int
from faan.x.util.timestamp import TimeStamp
from faan.core.model.general.deviceidentity import DeviceIdentity
from faan.core.model.general.device import Device
from copy import deepcopy
import os
from faan.core.model.general.geo import GeoCountry, GeoCity, GeoRegion, Geo
from faan.core.model.general.requestlog import RequestLog
from faan.admin.lib.videoconverter import resolutions
from faan.api.lib.mobfox import mobfoxRequest
from faan.core.model.security.partner_group import PartnerGroup
from faan.core.model.transaction import Transaction
from zlib import adler32 as crc
from faan.api.lib.gmaps import geoForCoordsX


@Controller
class IndexController():
    
    @Route("/q")
    def xq(self):
        #try:
        #    mobfoxRequest()
        #except:
        #    capp.logger.exception()
        
        app         = Application.One(uuid = request.values['app'])
        zones       = Zone.All( Zone.uuid.in_(request.values.getlist('zone'))
                              , state = Zone.State.ACTIVE)
        
        
        cached_geo  = request.values.get('geo')
        
        if cached_geo:
            country, region, city = map(int, cached_geo.split(":"))
        else:
            #if request.values.get('latitude') and request.values.get('longitude'):
            #    geoForCoordsX(request.values.get('latitude'), request.values.get('longitude'))
            
            pairs          = []
            
            geo_ipgb_code  = request.environ.get('GEO_IPGB_CODE', '').replace('-', '')

            pairs.append((request.values.get('country', ''), request.values.get('city', ''), "MOBILE"))
            pairs.append((geo_ipgb_code[0:2], geo_ipgb_code[2:], "IPGB"))
            pairs.append((request.environ.get('GEO_MM_COUNTRY', ''), request.environ.get('GEO_MM_CITY', ''), "MM"))

            city = region = country = None

            for s_country, s_city, s_src in pairs:
                if s_city:
                    city = GeoCity.Single(or_( GeoCity.name == s_city 
                                             , GeoCity.ipgb_code == s_city))
                    
                if city:
                    #capp.logger.debug("GEO ACTIVATED [%s] CITY > [%s / %s] ### MOB [%s / %s]", s_src, s_country, s_city, request.values.get('country', ''), request.values.get('city', ''))
                    break
                
            if city:
                region  = GeoRegion.Key(city.region)
                country = GeoCountry.Key(region.country)
            else:
                for s_country, s_city, s_src in pairs:
                    if s_country:
                        country = GeoCountry.Single(or_( GeoCountry.name == s_country
                                                       , GeoCountry.code == s_country))
                    
                    if country:
                        #capp.logger.debug("GEO ACTIVATED [%s] COUNTRY > [%s / %s] ### MOB [%s / %s]", s_src, s_country, s_city, request.values.get('country', ''), request.values.get('city', ''))
                        break
            
            country     = country.id if country else 0
            region      = region.id if region else 0
            city        = city.id if city else 0
            
            if not country:
                #capp.logger.debug("GEO FAILED [%s / %s] > %s", request.values.get('country', ''), request.values.get('city', ''), pairs[1:])
                pass
        
        platform    = getattr(Application.Os, request.values['platform'].upper())
        devclass    = getattr(Campaign.DevClass, request.values['devclass'].upper())
        
        mls         = request.values.get('mlog', '')
        
        medialog    = {}
        
        resp        = { 'zones'    : []
                      , 'medias'   : []
                      , 'data'     : {}
                      , 'settings' : {}
                      }

        resp['data']['geo'] = "%s:%s:%s" % ( country, region, city)


        # block any requests on old interface
        return jsonify( code = 0 
               , data = resp
               )


        if  app.state != Application.State.ACTIVE:                  # disable for inactive
            jsonify( code = 0 
                   , data = resp
                   )
        
        publisher   = Account.Key(app.account)
        pub_group   = PartnerGroup.Key(publisher.partner_group_id)
        
        pub_delta   = publisher.request_cost if publisher.request_cost is not None else pub_group.request_cost

           
        blocked_mids = []                                           # block overused medias        
        media_ids    = []
        
        if mls:
            for ml in mls.split('/'):                                              
                mid, daily, sess = map(int, ml.split(':'))
                medialog[mid] = { 'daily' : daily
                                , 'sess'  : sess }
            
            used_medias = Media.Filter(Media.id.in_(sorted(medialog.keys()))).order_by(Media.id).all()
            for um in used_medias:
                if  (um.daily_limit   and um.daily_limit   <=  medialog[um.id]['daily']) or \
                    (um.session_limit and um.session_limit <=  medialog[um.id]['sess'] ):
                    #capp.logger.debug("Blocking media [%s]", um.id)
                    blocked_mids.append(um.id)
        
        device_id = request.values.get('dev')
        
        if not device_id:
            identities  = {}                                            # detect device and apply device identities and properties
            devices     = {}
            properties  = {}
            didclauses  = []
            maxval      = 0
            maxdid      = None
            
            for k in [s[3:] for s in request.values.keys() if s.startswith('id_')]:
                ktype = getattr(DeviceIdentity.Type, k.upper())
                identities[ktype] = request.values['id_' + k]
                didclauses.append( and_( DeviceIdentity.type  == ktype
                                       , DeviceIdentity.value == identities[ktype]) )
                
            dids = DeviceIdentity.Filter(or_(*didclauses)) \
                                 .order_by(DeviceIdentity.id) \
                                 .all()
                                 
            for did in dids:
                didval = devices[did.device] = devices.get(did.device, 0) + 1
                try:
                    del identities[did.type]
                except:
                    pass
                
                if didval > maxval:
                    maxdid = did.device
                    maxval = didval
            
            device = Device.Key(maxdid) if maxdid else Device()
            device.properties = {}
            device.timestamp = TimeStamp.now()
            Session.add(device)
            Session.flush()
            Session.commit()
    
            for did in dids:
                did.device = device.id
                Session.add(did)
                
            for ktype, value in identities.items():
                did = DeviceIdentity()
                did.device  = device.id
                did.type    = ktype
                did.value   = value
                Session.add(did)
            
            for k in [s[5:] for s in request.values.keys() if s.startswith('prop_')]:
                properties[k] = request.values['prop_' + k]
            
            props = (device.properties or {})
            props.update(properties)
            device = Device.Key(device.id)              # WEIRD
            device.properties = deepcopy( props )
            
            #capp.logger.debug("Updating [%s] props to [%s]", device.id, props)
            
            Session.add(device)
            Session.flush()
            Session.commit()
            
            device_id = device.id
        
        resp['data']['dev'] = device_id
        
        
        # Geo targets
        
        geos = [Geo(country=Geo.COUNTRY_ALL, region=Geo.REGION_ALL, city=Geo.CITY_ALL).value]                  # ALL / ALL
        
        if country:
            geos.append(Geo(country=country, region=Geo.REGION_ALL, city=Geo.CITY_ALL).value)               # Country / All
            if not city:
                geos.append(Geo(country=country).value)                              # Country / Undetected
        
        if region:
            geos.append(Geo(country=country, region=region, city=Geo.CITY_ALL).value)       
        
        if city:
            geos.append(Geo(country=country, region=region, city=city).value)                  # Country / City
            #geos.append(Geo(country=city.country, city=Geo.CITY_ALL).value)             # Country / All - might be useless
        
        # capp.logger.debug("GEO >> [%s / %s] >> %s | %s | %s", s_country, s_city, country, region, city)    

        # log request
        
        rlog = RequestLog()
        
        rlog.device         = device_id
        rlog.application    = app.id
        rlog.devclass       = devclass
        rlog.platform       = platform
        rlog.country        = country
        rlog.region         = region
        rlog.city           = city
        rlog.timestamp      = TimeStamp.now()
        
        Session.add(rlog)
        Session.flush()
        Session.commit()
                
        sdkv = map(int, request.values.get('sdkv', '0.0.0').split('.'))
        sdkv = sdkv[0] * 10000 + sdkv[1] * 100 + sdkv[2]
        
        if sdkv < 10201:
            return jsonify( code = 0 
                          , data = resp
                          )            

        
        # query time
        
        q = Session.query(Account, Campaign, Media)                  # Tables
        
        q = q.filter(Account.id     == Campaign.account,             # ON clauses 
                     Campaign.id    == Media.campaign)        
    
        if blocked_mids:
            q = q.filter(not_(Media.id.in_(sorted(blocked_mids))))   # exclude those that reached limits
    
        q = q.filter(Account.state  == Account.State.ACTIVE,         # ACTIVE only
                     Campaign.state == Campaign.State.ACTIVE,
                     Media.state    == Media.State.ACTIVE)
        
        q = q.filter(Account.balance > Decimal(0))                   # Positive balance
        
        q = q.filter(Campaign.category.in_(app.target_categories))
        
        
        q = q.filter(or_( Campaign.targeting.op("&")(Campaign.Targeting.CATEGORY) == 0,
                          and_( Campaign.target_categories.op('&&')(app.category)
                              , Any(app.content_rating, Campaign.target_content_rating) )
                    ))
        
        q = q.filter(or_( Campaign.targeting.op("&")(Campaign.Targeting.PLATFORM) == 0,
                          Any(platform, Campaign.target_platforms)
                    ))

        q = q.filter(or_( Campaign.targeting.op("&")(Campaign.Targeting.DEVCLASS) == 0,
                          Any(devclass, Campaign.target_devclasses)
                    ))

        q = q.filter(or_( Campaign.targeting.op("&")(Campaign.Targeting.GEO) == 0,
                          Campaign.target_geo.op('&&')(geos)
                    ))
        
        
        for zone in zones:
            zq = q.filter(Media.duration >= zone.min_duration,
                          Media.duration <= zone.max_duration,
                          Media.closable <= zone.closable)
            
            zq = zq.order_by(desc((Media.cost + 10000) / (Media.raw + 1000)))     # TODO: improve ordering 
            
            (account, campaign, media) = zq.first() or (None, None, None)
            resp['zones'].append({ 'id'          : zone.uuid
                                 , 'v4vc_reward' : zone.v4vc_cpv
                                 , 'media'       : str(media.id) if media else None
                                 , 'closable'    : zone.closable
                                 , 'modFactor'   : zone.mod_factor
                                 , 'sessionLimit': zone.session_limit
                                 })

            if media and media.id not in media_ids:
                media_ids.append(media.id)
                
                xm =   { 'id'       : str(media.id)
                       #, 'name'     : media.name
                       , 'assets'   : {}
                       , 'uri'      : "http://api.vidiger.com/x?media=%s&zone=%s" % (media.id, zone.uuid)
                       #, 'campaign' : campaign.id
                       #, 'account'  : account.id
                       , 'closable' : media.closable
                       }
                
                media_upath = 'upload/%s/%s/%s/' % (account.id, campaign.id, media.id)
                media_lpath = os.path.join(capp.config['GLOBAL_FOLDER'], media_upath)
                
                vres = (800, 480)
                screen = request.values.get('screen')
                
                if screen:
                    sres = sorted(map(int, screen.split('x')))
                    sres.reverse()
                    
                    for r in resolutions:
                        if r[0] <= sres[0] and r[1] <= sres[1]:
                            vres = r
                            break
                
                vres_s = "%s_%s" % vres
                video_bpath = media_upath + 'video_' + vres_s 
                
                xm['assets']['video'] = { 'name'     : 'video'
                                        , 'media'    : str(media.id)   
                                        , 'uri'      : '/global/' + video_bpath
                                        , 'metrics'  : { 'duration' : media.duration } 
                                        , 'size'     : os.stat(media_lpath + 'video_' + vres_s).st_size 
                                        }
                
                xm['assets']['endcard'] = { 'name'   : 'endcard'
                                          , 'media'  : str(media.id)
                                          , 'uri'    : media.endcard or '/global/static/img/default-endcard.png'
                                          , 'size'   : os.stat(media_lpath + 'endcard').st_size if media.endcard else os.stat(os.path.join(capp.config['GLOBAL_FOLDER'], '/static/img/default-endcard.png')).st_size
                                          }
                
                if media.overlay:
                    xm['assets']['overlay'] = { 'name'  : 'overlay'
                                              , 'media' : str(media.id)
                                              , 'uri'   : media.overlay
                                              , 'size'  : os.stat(media_lpath + 'overlay').st_size 
                                              }
                    
    
                xm['hash'] = crc(str(xm))       # probably should be moved to users 
    
                resp['medias'].append(xm)

        if not resp['medias'] and pub_delta:
            Account.Filter(id=publisher.id).update({Account.balance : Account.balance + pub_delta})

        return jsonify( code = 0 
                      , data = resp
                      )

    @Route("/notify")
    def notify(self):
        return "OK"         # XXX: prevent any event reports
        
        
        #uuid        = request.values['uuid']
        
        zone        = Zone.One(uuid = request.values['zone'])
        application = Application.Key(zone.application)
        publisher   = Account.Key(application.account)
        pub_group   = PartnerGroup.Key(publisher.partner_group_id)
        
        media       = Media.One(id = request.values['media'])
        campaign    = Campaign.Key(media.campaign)
        advertiser  = Account.Key(campaign.account)
        adv_group   = PartnerGroup.Key(advertiser.partner_group_id)
        
        t           = request.values['type']
        country, region, city = map(int, (request.values.get('geo') or "0:0:0").split(':'))
        
        pst          = PubStat.Default()
        pst.account  = publisher.id
        pst.application = application.id
        pst.zone     = zone.id
        
        ast          = AdvStat.Default()
        ast.account  = advertiser.id
        ast.campaign = campaign.id
        ast.media    = media.id
        
        pst.country  = ast.country = country
        pst.region   = ast.region  = region
        pst.city     = ast.city    = city 
       
        adv_delta    = Decimal(0)                                   # MUST BE NEGATIVE IN BALANCE UPDATE
        pub_delta    = Decimal(0)
        
        if t == "raw":
            ast.raw = pst.raw = 1
            
            zone.raw         += 1                                   # TODO: rewrite using single "update by id" clause
            application.raw  += 1
            media.raw        += 1
            campaign.raw     += 1
        elif t == "complete":
            ast.impressions = pst.impressions = 1
            
            adv_delta                = campaign.bid 
            pub_delta                = campaign.bid * ((publisher.rate if publisher.rate is not None else pub_group.rate) / Decimal(100))
            
            ast.cost                 = adv_delta
            pst.revenue              = pub_delta
            
            zone.impressions         += 1
            application.impressions  += 1
            zone.v4vc_cost           += zone.v4vc_cpv
            application.v4vc_cost    += zone.v4vc_cpv
            
            media.impressions        += 1
            campaign.impressions     += 1

            zone.revenue             += pub_delta                          
            application.revenue      += pub_delta
            media.cost               += adv_delta
            campaign.cost            += adv_delta
        elif t == "overlay":
            ast.overlays = pst.overlays = 1
            
            adv_delta                 = advertiser.overlay_cost if advertiser.overlay_cost is not None else adv_group.overlay_cost
            pub_delta                 = publisher.overlay_cost  if publisher.overlay_cost  is not None else pub_group.overlay_cost
            
            ast.overlay_cost=ast.cost = adv_delta
            pst.revenue               = pub_delta
            
            media.overlays           += 1
            campaign.overlays        += 1

            zone.revenue             += pub_delta
            application.revenue      += pub_delta
            media.cost               += adv_delta
            campaign.cost            += adv_delta
            media.overlay_cost       += adv_delta
            campaign.overlay_cost    += adv_delta
        elif t == "endcard":
            ast.endcards = pst.endcards = 1
            
            if media.endcard:
                adv_delta                 = advertiser.endcard_cost if advertiser.endcard_cost is not None else adv_group.endcard_cost
                pub_delta                 = publisher.endcard_cost  if publisher.endcard_cost  is not None else pub_group.endcard_cost

            ast.endcard_cost=ast.cost = adv_delta
            pst.revenue               = pub_delta

            media.endcards        += 1
            campaign.endcards     += 1

            zone.revenue             += pub_delta
            application.revenue      += pub_delta
            media.cost               += adv_delta
            campaign.cost            += adv_delta
            media.endcard_cost       += adv_delta
            campaign.endcard_cost    += adv_delta

        capp.logger.debug("BALANCE DELTAs: Adv [%s], Pub[%s]", adv_delta, pub_delta)

        if pub_delta:
            Account.Filter(id=publisher.id) .update({Account.balance : Account.balance + pub_delta})           # TODO: probably, should run in a transaction

            transaction = Transaction.Default()

            transaction.ts_spawn    = TimeStamp.now()
            transaction.type        = Transaction.Type.IMPRESSION
            transaction.dst_acc     = publisher.id
            transaction.amount      = pub_delta

            Session.add(transaction)
                    
            Session.flush()
            Session.commit()
            
        if adv_delta:
            Account.Filter(id=advertiser.id).update({Account.balance : Account.balance - adv_delta})
            
            transaction = Transaction.Default()

            transaction.ts_spawn    = TimeStamp.now()
            transaction.type        = Transaction.Type.IMPRESSION
            transaction.dst_acc     = advertiser.id
            transaction.amount      = -adv_delta

            Session.add(transaction)
                    
            Session.flush()
            Session.commit()
                    
        Session.add(zone)
        Session.add(application)
        Session.add(media)
        Session.add(campaign)
        
        Session.flush()
        Session.commit()

            
        PubStat.StatCount(pst)
        AdvStat.StatCount(ast) 
        
        return "OK"

    @Route("/x")
    def click(self):
        #uuid        = request.values['uuid']
        
        zone        = Zone.One(uuid = request.values['zone'])
        application = Application.Key(zone.application)
        publisher   = Account.Key(application.account)
        
        media       = Media.One(id = request.values['media'])
        campaign    = Campaign.Key(media.campaign)
        advertiser  = Account.Key(campaign.account)
        
        country, region, city = map(int, (request.values.get('geo') or "0:0:0").split(':'))

        ast          = AdvStat.Default()
        ast.country  = country
        ast.region   = region
        ast.city     = city 
        ast.account  = advertiser.id
        ast.campaign = campaign.id
        ast.media    = media.id
       
        ast.clicks = 1
        media.clicks += 1
        campaign.clicks += 1
        
        Session.add(zone)
        Session.add(application)
        Session.add(media)
        Session.add(campaign)
        
        Session.flush()
        Session.commit()

            
        AdvStat.StatCount(ast)
        
        return redirect(media.uri)




















