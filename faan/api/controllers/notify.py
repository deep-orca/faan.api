from faan.core.X.xflask import Controller, Route, request, capp
from flask import Response
from faan.core.model.pub.application import Application
from faan.core.model.pub.unit import Unit
from faan.core.model.pub.source.source import Source
from faan.core.model.meta import Session
from faan.core.model.adv.campaign import Campaign
from faan.x.util.timestamp import TimeStamp
from faan.core.model.adv.media import Media
from faan.core.model.security.account import Account
from faan.core.model.security.partner_group import PartnerGroup
from faan.core.model.pub.statistics import PubStat
from faan.core.model.adv.statistics import AdvStat
from decimal import Decimal
from faan.core.model.transaction import Transaction
import redis


@Controller("/new/notify")
class NotifyController():
    
    @Route("/<event>")
    def notify(self, event):        # event : [ IMPRESSION | CLICK ]
        
        source      = Source.Key(request.values['source']) # Source.Key(request.values['source'])
        unit        = Unit.Key(source.unit) 
        application = Application.Key(unit.application)
        publisher   = Account.Key(application.account)
        pub_group   = PartnerGroup.Key(publisher.partner_group_id)
        
        country, region, city = map(int, (request.values.get('geo') or "0:0:0").split(':'))
        
        pst          = PubStat.Default()
        pst.account  = publisher.id
        pst.application = application.id
        pst.unit     = unit.id
        pst.source   = source.id
        pst.country  = country
        pst.region   = region
        pst.city     = city         

        if   event == "impression":
            pst.impressions = 1
        elif event == "click":
            pst.clicks = 1

        if request.values.get('media'):
            ast          = AdvStat.Default()        # stub
            media        = Media.Key(request.values['media'])
            campaign     = Campaign.Key(media.campaign)
            advertiser   = Account.Key(campaign.account)
            ast.account  = advertiser.id
            ast.campaign = campaign.id
            ast.media    = media.id
            ast.country  = country
            ast.region   = region
            ast.city     = city
                
            adv_delta    = Decimal(0)                                   # MUST BE NEGATIVE IN BALANCE UPDATE
            pub_delta    = Decimal(0)

            if   event == "click":
                ast.clicks = 1            
            elif event == "impression":
                ast.impressions = 1
                
                adv_delta                = campaign.bid 
                pub_delta                = campaign.bid * ((publisher.rate if publisher.rate is not None else pub_group.rate) / Decimal(100))
                ast.cost                 = adv_delta
                pst.revenue              = pub_delta
    
                media.applyRankDeltas(adv_delta, 1)  
                campaign.accountImpression(1)
                self.registerMediaEvent()
    
            if pub_delta:   
                self.balanceDelta(publisher.id, pub_delta)
                
            if adv_delta:   
                self.balanceDelta(advertiser.id, -adv_delta)
                campaign.accountCost(adv_delta)
            
            Session.flush()
            Session.commit()
         
            Session.add(media)
            Session.add(campaign)
            AdvStat.StatCount(ast) 
                
        PubStat.StatCount(pst)
        
        resp = ""
        
        if request.values.get("layer") == "web":
            resp = Response(resp, content_type="application/javascript")

        return resp
    
    def registerMediaEvent(self):
        if not request.values.get('dev') or not request.values.get('media') or not request.values.get('session'):
            return
        
        r    = redis.StrictRedis(host = "vidiger.com")
        
        rkey = "media_log_%s" % request.values.get('dev')
        mls  = r.get(rkey) or ""
        mls += (" " if mls else "") + "%s:%s:%s" % (request.values.get('media'), TimeStamp.now(), request.values.get('session')) 
       
        r.set(rkey, mls, ex = 24 * 60 * 60)
        
    
    def balanceDelta(self, account, delta):
        Account.Filter(id=account).update({Account.balance : Account.balance + delta})
        
        transaction = Transaction.Default()

        transaction.ts_spawn    = TimeStamp.now()
        transaction.type        = Transaction.Type.IMPRESSION
        transaction.dst_acc     = account
        transaction.amount      = delta

        Session.add(transaction)
                
        Session.flush()
        Session.commit()
                     
    
    
    
    
    
    
    
