from faan.core.X.xflask import Controller, Route, capp
from faan.core.model.adv.campaign import Campaign
from faan.core.model.meta import Session
from faan.core.model.adv.media import Media


@Controller("/periodic")
class PeriodicController():
    
    @Route("/daily")
    def daily(self):
        capp.logger.info(">>> DAILY TASKS")
        
        Campaign.ResetDailyReach()                              # reset daily reach for campaigns
        Session.commit()

        Media.ShiftRanks()                                      # diminish ranking values
        Session.commit()
       
        capp.logger.info("<<< DAILY TASKS")
        
        return ''


    @Route("/hourly")
    def hourly(self):
        capp.logger.info(">>> HOURLY TASKS")

        # nothing to do yet
        
        capp.logger.info("<<< HOURLY TASKS")
        
        return ''











