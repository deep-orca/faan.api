from faan.core.X.xflask import Controller, Route, request, capp
from faan.core.model.adv.campaign import Campaign
from faan.core.model.adv.media import Media
from faan.core.model.general.geo import Geo
from faan.core.model.meta import Session
from faan.core.model.pub.application import Application
from faan.core.model.pub.source.source import Source
from faan.core.model.pub.unit import Unit
from urllib import urlencode
from flask import json, redirect, g
from flask.wrappers import Response
from sqlalchemy.sql.expression import asc, desc, or_, not_, and_
from sqlalchemy.dialects.postgresql.base import Any
from faan.api.lib.xheaders import XHEADERS
from faan.core.model.security.account import Account
from faan.api.lib.extract import EXTRACTORS
from decimal import Decimal
import redis
from faan.x.util.timestamp import TimeStamp
from faan.core.X.mako import render_template
import os
from faan.admin.lib.videoconverter import resolutions


@Controller("/new/query")
class QueryController():
    
    PERFECT_ECPM        = Decimal(100)
    PERFECT_SAMPLING    = 1 
    RANKING_CLAUSE      = Media.RANK_CLAUSE(PERFECT_ECPM, PERFECT_SAMPLING)     # put here to prevent re-evaluation
     
     
    @Route("/web")
    def query_web(self):
        g.layer = "web"
        
        resp = self.query()
        
        data = { }
        
        for hk, hv in resp.headers:
            dk = hk[2:] if hk.startswith("X-") else hk
            data[dk.lower().replace("-", "_")] = hv
        
        data["ad_parameters"] = data.get("ad_parameters") and json.loads(data.get("ad_parameters"))  or {} 
        
        ukey = '"' + str(g.unit.id) + "-" + str(g.unit.uuid) + '"'
         
        js  = ""
        js += "window.vdgr.units[" + ukey + "].ad = " + json.dumps(data) + ";"
        
        if XHEADERS.AD_CLASS in resp.headers:
            js += "window.vdgr.units[" + ukey + "].ad.ad_class = " + resp.headers[XHEADERS.AD_CLASS] + ";"
        
        if XHEADERS.APP_CACHE in resp.headers:  
            js += "window.vdgr.cache = " + resp.headers[XHEADERS.APP_CACHE] + ";"
        
        if XHEADERS.UNIT_CACHE in resp.headers:  
            js += "window.vdgr.units[" + ukey + "].cache = " + resp.headers[XHEADERS.UNIT_CACHE] + ";"
        
        if resp.status_code == 200:
            js += "window.vdgr.units[" + ukey + "].ad.ad_parameters.body = " + json.dumps(resp.data) + ";"
        
        js += "window.vdgr.units[" + ukey + "].render();"
        
        jresp = Response(js, status=200, content_type="application/javascript")
        
        for k, v in g.cookies.items():
            jresp.set_cookie(k, **v)
        
        return jresp
    
            
    @Route("/sdk")
    def query_sdk(self):       
        g.layer = "sdk"
        return self.query()


    def query(self):
        g.app_cache     = { }
        g.unit_cache    = { }
        g.cookies       = { }
        
        uid, ukey = request.values['unit'].split("-")
        
        g.unit = Unit.Key(uid)
        g.app  = Application.Key(g.unit.application)
        
        if g.unit.uuid != ukey:
            #capp.logger.debug(" CLEAR >>> UNIT uuid MISMATCH")
            return self.clear()
        
        if  g.app.state  != Application.State.ACTIVE or \
            g.unit.state != Unit.State.ACTIVE:
            #capp.logger.debug(" CLEAR >>> STATE != ACTIVE")
            return self.clear() 
        
        
        [E.Extract() for E in EXTRACTORS]
        
        self.selectSource()

        if not g.source:
            #capp.logger.debug(" CLEAR >>> NOT g.source")
            return self.clear()
        
        # build response
        
        resp = self.build_response()
        
        resp.headers[XHEADERS.CLICKTHROUGH_URL] = self.build_clickthrough_url()
        resp.headers[XHEADERS.FAIL_URL]         = self.build_fail_url()
        resp.headers[XHEADERS.IMPRESSION_URL]   = self.build_impression_url()
        
        resp.headers[XHEADERS.APP_CACHE]        = json.dumps(g.app_cache)
        resp.headers[XHEADERS.UNIT_CACHE]       = json.dumps(g.unit_cache)
        
        return resp
        
    
    def selectSource(self):
        g.source = None
        g.media  = None
        g.ecpm   = None
        
        g.excl_src = map(int, filter(bool, request.values.get('excl_src', '').split('/')))
        g.excl_src.sort()
        
        sources_q = Source.Query()\
                          .filter(Source.unit == g.unit.id)\
                          .filter(Source.state == Source.State.ACTIVE)
                          
        if g.excl_src:
            sources_q = sources_q.filter(~(Source.id.in_(g.excl_src)))
        
        sources_q = sources_q.order_by(Source.ecpm)
        
        sources = sources_q.all()
        if not sources:
            return
        
        g.source    = sources[0]
        g.account   = None
        g.campaign  = None
        g.media     = None
        g.ecpm      = g.source.ecpm
        
        for src in sources:
            ecpm  = src.ecpm
            media = None 
            
            if src.type < 100:  # estimate ecpm
                ecpm, account, campaign, media = self.internal_q(src)
                
            if ecpm > g.ecpm:
                g.source    = src
                g.account   = account
                g.campaign  = campaign
                g.media     = media
                g.ecpm      = ecpm
                
        g.excl_src.append(g.source.id)
        
        if g.source.type < 100 and not g.media:
            g.source = None
        

    def internal_q(self, src):
        platform    = getattr(Application.Platform, request.values['platform'].upper())
        devclass    = getattr(Campaign.DevClass, request.values['devclass'].upper())
        
        self.registerMLog()
        
        geos = [Geo(country=Geo.COUNTRY_ALL, region=Geo.REGION_ALL, city=Geo.CITY_ALL).value]                 # ALL / ALL
        
        if g.country:

            geos.append(Geo(country=g.country, region=Geo.REGION_ALL, city=Geo.CITY_ALL).value)               # Country / All
            if not g.city:
                geos.append(Geo(country=g.country).value)                              # Country / Undetected
        
        if g.region:
            geos.append(Geo(country=g.country, region=g.region, city=Geo.CITY_ALL).value)       
        
        if g.city:
            geos.append(Geo(country=g.country, region=g.region, city=g.city).value)                  # Country / City
        
        q = Session.query(Account, Campaign, Media)                  # Tables
        
        q = q.filter(Account.id     == Campaign.account,             # ON clauses 
                     Campaign.id    == Media.campaign)        
    
        if g.blocked_mids:
            q = q.filter(~(Media.id.in_(sorted(g.blocked_mids))))   # exclude those that reached limits
    
        if request.values.get('debug'):
            q = q.filter(Account.state  == Account.State.ACTIVE,    # ACTIVE only / DEBUG
                         Campaign.state == Campaign.State.ACTIVE)
        else:
            q = q.filter(Account.state  == Account.State.ACTIVE,    # ACTIVE only
                         Campaign.state == Campaign.State.ACTIVE,
                         Media.state    == Media.State.ACTIVE)            
            
        q = q.filter(Account.balance > Decimal(0))                   # Positive balance
         
        # Targetings
         
        q = q.filter(Campaign.category.in_(g.app.target_categories))
          
        q = q.filter( (Campaign.targeting.op("&")(Campaign.Targeting.CATEGORY) == 0) | ((Campaign.target_categories.op('&&')(g.app.category)) & ( Any(g.app.content_rating, Campaign.target_content_rating))))
        q = q.filter( (Campaign.targeting.op("&")(Campaign.Targeting.PLATFORM) == 0) | (Any(platform, Campaign.target_platforms)) )
        q = q.filter( (Campaign.targeting.op("&")(Campaign.Targeting.DEVCLASS) == 0) | (Any(devclass, Campaign.target_devclasses)) )
        q = q.filter( (Campaign.targeting.op("&")(Campaign.Targeting.GEO) == 0)      | (Campaign.target_geo.op('&&')(geos)) )
        # Whitelist/Blacklist
        q = q.filter( (Campaign.target_applications.any(g.app.id))                   | (Campaign.target_applications == []) )
        
        # limits
        
        q = q.filter( (Campaign.limit_cost_daily == 0) | (Campaign.limit_cost_daily > Campaign.reach_cost_daily) )
        q = q.filter( (Campaign.limit_cost_total == 0) | (Campaign.limit_cost_total > Campaign.reach_cost_total) )
        q = q.filter( (Campaign.limit_imp_daily  == 0) | (Campaign.limit_imp_daily  > Campaign.reach_imp_daily)  )
        q = q.filter( (Campaign.limit_imp_total  == 0) | (Campaign.limit_imp_total  > Campaign.reach_imp_total)  )
        
        if g.unit.type == Unit.Type.INTERSTITIAL:
            q = q.filter(Media.placement == Media.Placement.INTERSTITIAL)
            
            q = q.filter( (Media.type != Media.Type.VIDEO) |
                          ((Media.duration >= src.parameters['min_duration']) &
                           (Media.duration <= src.parameters['max_duration']) &
                           (Media.closable <= src.parameters['closable'])) 
                        )
        else:
            q = q.filter(Media.placement == Media.Placement.BANNER)
            
            if request.values.get('bbox'):
                bbox = map(int, request.values['bbox'].split('-'))
                bbox.sort(reverse = g.unit.width > g.unit.height)
            else:
                bbox = [0, 0]
                
            if bbox[0] > g.unit.width and bbox[1] > g.unit.height:
                q = q.filter((Media.width >= g.unit.width) & (Media.height >= g.unit.height))
                q = q.filter((Media.width <= bbox[0])      & (Media.height <= bbox[1]))
                
                capp.logger.debug("unit: [%s, %s], bbox: [%s, %s]", g.unit.width, g.unit.height, bbox[0], bbox[1])
            else:
                q = q.filter((Media.width == g.unit.width) & (Media.height == g.unit.height))
                
            q = q.filter((Media.type == Media.Type.BANNER) | (Media.type == Media.Type.MRAID))
            

        q = q.order_by(desc(self.RANKING_CLAUSE))     # TODO: improve ordering 
        
        (account, campaign, media) = q.first() or (None, None, None)

        if media:        
            return campaign.bid, account, campaign, media
        else: 
            return 0, None, None, None

    def registerMLog(self):
        g.blocked_mids = []
        
        if not g.device.id:
            return
        
        r = redis.StrictRedis(host = "vidiger.com")
        
        cts  = TimeStamp.now()
        rkey = "media_log_%s" % g.device.id
        
        mls  = r.get(rkey) or ""
        mls  = mls.split(' ') if mls else []
        
        #capp.logger.debug("MLOG DBG: %s", mls)
        
        mlevents = []
        mcntrs   = {}
        changed  = set()
       
        for ml in mls:
            mid, ts, sess = ml.split(':')
            mid = int(mid)
            ts  = int(ts)
            
            if  mid not in mcntrs:
                mcntrs[mid] = { 'd' : 0, 's' : 0 }
            
            if ts < cts - 24 * 60 * 60:
                continue
            
            mcntrs[mid]['d'] += 1
            
            if sess:
                if sess == g.session:
                    mcntrs[mid]['s'] += 1
                else:
                    sess = ""
            
            changed.add(mid)            
            mlevents.append("%s:%s:%s" % (mid, ts, sess))
             
        r.set(rkey, " ".join(mlevents), ex = 24 * 60 * 60)
        
        changed_medias = Media.Filter(Media.id.in_(sorted(list(changed)))).order_by(Media.id).all()
        for cm in changed_medias:
            if  (cm.daily_limit   and cm.daily_limit   <=  mcntrs[cm.id]['d']) or \
                (cm.session_limit and cm.session_limit <=  mcntrs[cm.id]['s']):
                #capp.logger.debug("Blocking media [%s]", um.id)
                g.blocked_mids.append(cm.id)
        

       
    def build_response(self):
        resp = redirect("http://localhost")
        
        resp.headers[XHEADERS.AD_PARAMETERS] = "{}";
        resp.headers[XHEADERS.AD_PLACEMENT]  = g.source.AD_PLACEMENT
        resp.headers[XHEADERS.AD_CLASS]      = g.source.AD_CLASS
        
        if g.source.type == Source.Type.VIDIGER:
            
            
            if g.media.type == Media.Type.MRAID:
                resp.headers[XHEADERS.AD_TYPE]       = "MRAID"
                if   g.media.mraid_type == Media.MRAID_Type.URL:
                    resp.headers[XHEADERS.LOCATION]      = g.media.mraid_url
                elif g.media.mraid_type == Media.MRAID_Type.FILE:
                    resp.headers[XHEADERS.LOCATION]      = "http://" + request.environ['HTTP_HOST'] + g.media.mraid_file
                elif g.media.mraid_type == Media.MRAID_Type.HTML:
                    resp.set_data(g.media.mraid_html)
                    resp.status_code    = 200
                    resp.content_type   = "text/html"
                    
                resp.headers[XHEADERS.AD_PARAMETERS] = json.dumps({ 'mraid'       : g.media.mraid_url
                                                                  , 'uri'         : g.media.uri
                                                                  , 'phone'       : g.media.phone_number
                                                                  , 'uri_t'       : g.media.uri_target
                                                                  , 'action'      : g.media.action_click 
                                                                  , 'unit_size'   : [g.unit.width,  g.unit.height]
                                                                  , 'banner_size' : [g.media.width, g.media.height]
                                                                  , 'script'      : "http://" + request.environ['HTTP_HOST'] + "/static/wrappers/mraid/" + g.source.AD_PLACEMENT.lower() + ".js"
                                                                  })
            elif g.media.type == Media.Type.BANNER:
                resp.headers[XHEADERS.AD_TYPE]       = "HTML"
                resp.headers[XHEADERS.AD_PARAMETERS] = json.dumps({ 'image'       : "http://" + request.environ['HTTP_HOST'] + g.media.banner
                                                                  , 'uri'         : g.media.uri
                                                                  , 'phone'       : g.media.phone_number
                                                                  , 'uri_t'       : g.media.uri_target
                                                                  , 'action'      : g.media.action_click 
                                                                  , 'unit_size'   : [g.unit.width,  g.unit.height]
                                                                  , 'banner_size' : [g.media.width, g.media.height]
                                                                  , 'script'      : "http://" + request.environ['HTTP_HOST'] + "/static/wrappers/" + g.source.AD_PLACEMENT.lower() + ".js"
                                                                  })
                resp.headers[XHEADERS.LOCATION]      = "http://" + request.environ['HTTP_HOST'] + "/static/wrappers/" + g.source.AD_PLACEMENT.lower() + ".html"  #g.media.mraid
            elif g.media.type == Media.Type.VIDEO:
                media_upath = '/upload/%s/%s/%s/' % (g.account.id, g.campaign.id, g.media.id)
                media_lpath = os.path.join(capp.config['GLOBAL_FOLDER'], media_upath)
                
                vres = (800, 480)
                screen = request.values.get('screen')
                
                if screen:
                    sres = sorted(map(int, screen.split('x')))
                    sres.reverse()
                    
                    for r in resolutions:
                        if r[0] <= sres[0] and r[1] <= sres[1]:
                            vres = r
                            break
                
                vres_s = "%s_%s" % vres
                video_bpath = media_upath + 'video_' + vres_s + '.mp4'
                
                resp.headers[XHEADERS.AD_TYPE]       = "MRAID"
                resp.headers[XHEADERS.AD_PARAMETERS] = json.dumps({ 'video'      : "http://" + request.environ['HTTP_HOST'] + video_bpath
                                                                  , 'uri'        : g.media.uri
                                                                  , 'phone'      : g.media.phone_number
                                                                  , 'uri_t'      : g.media.uri_target
                                                                  , 'overlay'    : g.media.overlay 
                                                                  , 'endcard'    : "http://" + request.environ['HTTP_HOST'] + g.media.endcard
                                                                  , 'closable'   : g.media.closable 
                                                                  , 'action_end' : g.media.action_end
                                                                  , 'action_clk' : g.media.action_click 
                                                                  , 'duration'   : g.media.duration
                                                                  , 'script'     : "http://" + request.environ['HTTP_HOST'] + "/static/wrappers/video.js"
                                                                  })
                
                resp.set_data(render_template("wrappers/interstitial/mraid/video.html", params = resp.headers[XHEADERS.AD_PARAMETERS]))
                resp.status_code    = 200
                resp.content_type   = "text/html"
                
                #resp.headers[XHEADERS.LOCATION]             = "http://" + request.environ['HTTP_HOST'] + "/static/wrappers/video.html"  #g.media.mraid                      
                
        elif  g.source.type == Source.Type.MEDIATION_ADMOB:
            resp.set_data(g.media.mraid_html)
            resp.status_code    = 200
            resp.content_type   = "text/html"
            
            resp.headers[XHEADERS.AD_PARAMETERS] = g.source.parameters
            
        if g.unit.type == Unit.Type.BANNER:
            resp.headers[XHEADERS.REFRESH_TIME] = g.unit.refresh_rate
            resp.headers[XHEADERS.AD_TIMEOUT] = 10
        
        return resp
    
    
    
    def build_fail_url(self):
        fail_params = dict(request.values.items())
        fail_params['excl_src'] = "/".join(map(str, g.excl_src))
       
        fail_url = "http://" + request.environ['HTTP_HOST'] + "/new/query/" + g.layer + "?" + urlencode(fail_params)

        return fail_url

    
    def build_notify_url(self, event):
        params = { 'unit'   : g.unit.id
                 , 'source' : g.source.id
                 , 'media'  : g.media.id if g.media else ''
                 , 'layer'  : g.layer 
                 }
        
        params.update(g.app_cache)
        
        return "http://" + request.environ['HTTP_HOST'] + "/new/notify/" + event + "?" + urlencode(params) 

    def build_clickthrough_url(self):
        return self.build_notify_url("click")

    def build_impression_url(self):
        return self.build_notify_url("impression")

    def clear(self):
        resp = Response('', status=200, headers=[])
        resp.headers[XHEADERS.CLEAR] = 'clear'

        if g.unit.type == Unit.Type.BANNER:
            resp.headers[XHEADERS.REFRESH_TIME] = g.unit.refresh_rate
            resp.headers[XHEADERS.AD_TIMEOUT] = 10        
        
        return resp

        
        
        
        
    

    
    
    













