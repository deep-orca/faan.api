from faan.core.X.xflask import Controller, Route, request, capp
from faan.core.model.meta import Session
from faan.core.model.adv.media import Media



@Controller("/util")
class UtilController():
    
    @Route("/converter_report")
    def converter_report(self):
        media = Media.Key(int(request.values['media']))
        
        if request.values['status'] == "OK":
            media.flags |= Media.Flag.CONVERTED
            
            Session.add(media)
            
            Session.flush()
            Session.commit()
            
        
        return 'OK'



    @Route("/environ")
    def environ(self):
        return "%s" % request.environ

    @Route("/geo/check")
    def geoCheck(self):
        pass

















