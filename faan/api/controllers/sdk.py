from faan.core.X.xflask import Controller, Route, request, capp
from faan.core.model.pub.application import Application
from faan.core.model.meta import Session
from faan.core.model.adv.campaign import Campaign
from faan.core.model.general.sdklog import SDKLog
from faan.x.util.func import try_int
from faan.x.util.timestamp import TimeStamp
from faan.core.model.pub.zone import Zone
from faan.core.model.general.geo import Geo



@Controller("/sdk")
class SDKController():
    
    @Route("/log", methods=["GET", "POST"])
    def log(self):
        
        entry = SDKLog.Default()
        
        entry.timestamp       = TimeStamp.now()
        entry.identities      = "#".join(request.values.getlist('identity')) # TODO: change this to actual identity parsing
    
        app = request.values.get('app')
        app = app and Application.Single(uuid = app)
    
        entry.application     = app.id if app else 0
        
        zone = request.values.get('zone')
        zone = zone and Zone.Single(uuid = zone)
        
        entry.zone            = zone.id if zone else 0
        
        entry.campaign        = try_int(request.values.get('campaign',  0))
        entry.media           = try_int(request.values.get('media',     0))
    
        entry.platform        = getattr(Application.Os, request.values.get('platform', '').upper(), 0)
        entry.devclass        = getattr(Campaign.DevClass, request.values.get('devclass', '').upper(), 0)
        
        geo = map(int, request.values.get('geo', "0:0:0").split(':'))
        entry.geo             = Geo(country=geo[0], region=geo[1], city=geo[2]).value

        entry.type            = getattr(SDKLog.Type, request.values.get('type', '').upper(), 0)
        
        entry.device          = try_int(request.values.get('dev',       0))
        
        sdkv = map(int, request.values.get('sdkv', '0.0.0').split('.'))
        sdkv = sdkv[0] * 10000 + sdkv[1] * 100 + sdkv[2]

        entry.sdkv            = sdkv         

        entry.message         = request.values.get('message', '')
        entry.context         = request.values.get('context', '')

        Session.add(entry)
        Session.flush()
        Session.commit()
        
        return 'OK'






















