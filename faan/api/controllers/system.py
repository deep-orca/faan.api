from faan.core.X.xflask import Controller, Route, request, capp
from faan.core.model.pub.application import Application
from faan.core.model.pub.zone import Zone
from faan.core.model.meta import Session
from faan.core.model.adv.campaign import Campaign
from faan.core.model.security.account import Account
from faan.core.model.adv.media import Media
from decimal import Decimal
from sqlalchemy.dialects.postgresql.base import Any
from flask import json, redirect, jsonify
from faan.core.model.pub.statistics import PubStat
from faan.core.model.adv.statistics import AdvStat
from sqlalchemy.sql.expression import desc, or_, not_, and_
from faan.x.util.func import try_int
from faan.x.util.timestamp import TimeStamp
from faan.core.model.general.deviceidentity import DeviceIdentity
from faan.core.model.general.device import Device
from copy import deepcopy
import os
from faan.core.model.general.geo import GeoCountry, GeoCity, GeoRegion, Geo
from faan.core.model.general.requestlog import RequestLog
from faan.admin.lib.videoconverter import resolutions
from faan.api.lib.mobfox import mobfoxRequest
from faan.core.model.security.partner_group import PartnerGroup
from faan.core.model.transaction import Transaction
from zlib import adler32 as crc
from faan.api.lib.gmaps import geoForCoordsX


@Controller
class IndexController():
    
    @Route("/q")
    def xq(self):
        app         = Application.One(uuid = request.values['app'])
        zones       = Zone.All( Zone.uuid.in_(request.values.getlist('zone'))
                              , state = Zone.State.ACTIVE)
        
        cached_geo  = request.values.get('geo')
        
        if cached_geo:
            country, region, city = map(int, cached_geo.split(":"))
        else:
            #if request.values.get('latitude') and request.values.get('longitude'):
            #    geoForCoordsX(request.values.get('latitude'), request.values.get('longitude'))
            pairs          = []
            geo_ipgb_code  = request.environ.get('GEO_IPGB_CODE', '').replace('-', '')

            pairs.append((request.values.get('country', ''), request.values.get('city', ''), "MOBILE"))
            pairs.append((geo_ipgb_code[0:2], geo_ipgb_code[2:], "IPGB"))
            pairs.append((request.environ.get('GEO_MM_COUNTRY', ''), request.environ.get('GEO_MM_CITY', ''), "MM"))

            city = region = country = None

            for s_country, s_city, s_src in pairs:
                if s_city:
                    city = GeoCity.Single(or_( GeoCity.name == s_city 
                                             , GeoCity.ipgb_code == s_city))
                if city:
                    #capp.logger.debug("GEO ACTIVATED [%s] CITY > [%s / %s] ### MOB [%s / %s]", s_src, s_country, s_city, request.values.get('country', ''), request.values.get('city', ''))
                    break
                
            if city:
                region  = GeoRegion.Key(city.region)
                country = GeoCountry.Key(region.country)
            else:
                for s_country, s_city, s_src in pairs:
                    if s_country:
                        country = GeoCountry.Single(or_( GeoCountry.name == s_country
                                                       , GeoCountry.code == s_country))
                    if country:
                        #capp.logger.debug("GEO ACTIVATED [%s] COUNTRY > [%s / %s] ### MOB [%s / %s]", s_src, s_country, s_city, request.values.get('country', ''), request.values.get('city', ''))
                        break
            
            country     = country.id if country else 0
            region      = region.id if region else 0
            city        = city.id if city else 0
            
            if not country:
                #capp.logger.debug("GEO FAILED [%s / %s] > %s", request.values.get('country', ''), request.values.get('city', ''), pairs[1:])
                pass
        
        resp        = { 'zones'    : []
                      , 'medias'   : []
                      , 'data'     : {}
                      , 'settings' : {}
                      }

        resp['data']['geo'] = "%s:%s:%s" % ( country, region, city)

        # block any requests on old interface
        return jsonify( code = 0 
                      , data = resp
                      )

    @Route("/notify")
    def notify(self):
        return "OK"         # XXX: prevent any notifications on old interface
    




















