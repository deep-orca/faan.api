from faan.core.X.xflask import request,  g
from faan.core.model.general import Device, DeviceIdentity
from faan.x.util.timestamp import TimeStamp
from faan.core.model.meta import Session
from copy import deepcopy
from sqlalchemy.sql.expression import or_
import uuid


class SessionExtractor():

    @staticmethod
    def Extract():
        
        
        session_id = request.values.get('session') or request.cookies.get('session') or str(uuid.uuid4().get_hex().upper()[0:16])
        
        g.session = session_id
        
        g.app_cache['session'] = g.session
        g.cookies['session'] = { 'value' : g.session }
        
        


