from faan.core.X.xflask import request, capp, g
from faan.core.model.general.geo import Geo, GeoCity, GeoRegion, GeoCountry

class GeoExtractor():
    COOKIE_TTL = 60 * 60 * 2
    
    @staticmethod
    def Extract():
        cached_geo = request.values.get('geo')
        
        if cached_geo:
            country, region, city = map(int, cached_geo.split(":"))
        else:
            #if request.values.get('latitude') and request.values.get('longitude'):
            #    geoForCoordsX(request.values.get('latitude'), request.values.get('longitude'))
            
            pairs          = []
            
            geo_ipgb_code  = request.environ.get('GEO_IPGB_CODE', '').replace('-', '')

            pairs.append((request.values.get('country', ''), request.values.get('city', ''), "MOBILE"))
            pairs.append((geo_ipgb_code[0:2], geo_ipgb_code[2:], "IPGB"))
            pairs.append((request.environ.get('GEO_MM_COUNTRY', ''), request.environ.get('GEO_MM_CITY', ''), "MM"))

            city = region = country = None

            for s_country, s_city, s_src in pairs:
                if s_city:
                    city = GeoCity.Single((GeoCity.name == s_city) | (GeoCity.ipgb_code == s_city))
                    
                if city:
                    #capp.logger.debug("GEO ACTIVATED [%s] CITY > [%s / %s] ### MOB [%s / %s]", s_src, s_country, s_city, request.values.get('country', ''), request.values.get('city', ''))
                    break
                
            if city:
                region  = GeoRegion.Key(city.region)
                country = GeoCountry.Key(region.country)
            else:
                for s_country, s_city, s_src in pairs:
                    if s_country:
                        country = GeoCountry.Single((GeoCountry.name == s_country) | (GeoCountry.code == s_country))
                    if country:
                        #capp.logger.debug("GEO ACTIVATED [%s] COUNTRY > [%s / %s] ### MOB [%s / %s]", s_src, s_country, s_city, request.values.get('country', ''), request.values.get('city', ''))
                        break
            
            country     = country.id if country else 0
            region      = region.id if region else 0
            city        = city.id if city else 0
            
            if not country:
                pass
                #capp.logger.debug("GEO FAILED [%s / %s] > %s", request.values.get('country', ''), request.values.get('city', ''), pairs[1:])
        
        g.country, g.region, g.city = country, region, city
        
        g.app_cache['geo'] = "%s:%s:%s" % (country, region, city)
        g.cookies['geo'] = { 'value'    : "%s:%s:%s" % (country, region, city)
                           , 'max_age'  : GeoExtractor.COOKIE_TTL
                            
                           }
        
