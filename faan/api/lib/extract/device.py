from faan.core.X.xflask import request,  g
from faan.core.model.general import Device, DeviceIdentity
from faan.x.util.timestamp import TimeStamp
from faan.core.model.meta import Session
from copy import deepcopy
from sqlalchemy.sql.expression import or_
import uuid


class DeviceExtractor():
    COOKIE_TTL = 60 * 60 * 24 * 365
    COOKIE_KEY = 'id_' + DeviceIdentity.Type.inverse()[DeviceIdentity.Type.COOKIE_TAG].lower()

    @staticmethod
    def Extract():
        device_id = request.values.get('dev') or request.cookies.get('dev')
        
        if device_id:
            device = Device.Key(device_id)
        else:
            identities  = {}                                            # detect device and apply device identities and properties
            devices     = {}
            didclauses  = []
            maxval      = 0
            maxdid      = None
            dids        = []
            
            for k, v in [(s[3:], v) for s, v in request.values.items() + request.cookies.items() if s.startswith('id_')]:
                ktype = getattr(DeviceIdentity.Type, k.upper())
                identities[ktype] = v #request.values['id_' + k]
                didclauses.append( (DeviceIdentity.type  == ktype) & (DeviceIdentity.value == identities[ktype]) )
                
            if didclauses:
                dids = DeviceIdentity.Filter(or_(*didclauses)) \
                                     .order_by(DeviceIdentity.id) \
                                     .all()
                for did in dids:
                    didval = devices[did.device] = devices.get(did.device, 0) + 1
                    try:
                        del identities[did.type]
                    except:
                        pass
                    
                    if didval > maxval:
                        maxdid = did.device
                        maxval = didval
            
            device = Device.Key(maxdid) if maxdid else Device.Default()
            
            if dids or identities:
                device.timestamp = device.timestamp or TimeStamp.now()
    
                Session.add(device)
                Session.flush()
                Session.commit()
        
                for did in dids:
                    if did.device != device.id:
                        did.device = device.id
                        Session.add(did)
                    
                for ktype, value in identities.items():
                    did = DeviceIdentity()
                    did.device  = device.id
                    did.type    = ktype
                    did.value   = value
                    Session.add(did)
        
        # device acquired, populate properties now
        properties  = {}
        for k in [s[5:] for s in request.values.keys() if s.startswith('prop_')]:
            properties[k] = request.values['prop_' + k]
        
        props = device.properties
        props.update(properties)
        device.properties = deepcopy( props )
        
        if device.id:
            device = Device.Key(device.id)              # WEIRD
            device.properties = deepcopy( props )
            
            #capp.logger.debug("Updating [%s] props to [%s]", device.id, props)
            
            Session.add(device)
            Session.flush()
            Session.commit()
            
            g.app_cache['dev'] = str(device.id)
            g.cookies['dev'] = { 'value'    : str(device.id)
                               , 'max_age'  : DeviceExtractor.COOKIE_TTL
                               }
            
        g.device = device
        
        g.cookies[DeviceExtractor.COOKIE_KEY] = { 'value'    : request.cookies.get(DeviceExtractor.COOKIE_KEY) or str(uuid.uuid4().get_hex().upper())
                                                , 'max_age'  : DeviceExtractor.COOKIE_TTL
                                                }
        
        


