from faan.core.X import XEnum

class XHEADERS_ENUM(XEnum):
    def __init__(self):
        XEnum.__init__(self)
        self.AD_CLASS               = "X-AD-CLASS"                  
        self.AD_PARAMETERS          = "X-AD-PARAMETERS"                 # JSON Dict
        self.AD_PLACEMENT           = "X-AD-PLACEMENT"                  # INTERSTITIAL / BANNER
        self.AD_TIMEOUT             = "X-AD-TIMEOUT"
        self.CLICKTHROUGH_URL       = "X-CLICKTHROUGH-URL"
        self.IMPRESSION_URL         = "X-IMPRESSION-URL"
        self.FAIL_URL               = "X-FAIL-URL"
        self.CLEAR                  = "X-CLEAR"
        self.REFRESH_TIME           = "X-REFRESH-TIME"
       
        self.APP_CACHE              = "X-APP-CACHE"
        self.UNIT_CACHE             = "X-UNIT-CACHE"
    
        self.AD_TYPE                = "X-AD-TYPE"                       # MRAID / VAST / VIDEO / IMAGE
        self.DSP_CREATIVE_ID        = "X-DSP-CREATIVE-ID"
        self.FULL_AD_TYPE           = "X-FULL-AD-TYPE"
        self.REDIRECT_URL           = "X-REDIRECT-URL"
        self.NATIVE_PARAMS          = "X-NATIVE-PARAMS"
        self.NETWORK_TYPE           = "X-NETWORK-TYPE"
        self.SCROLLABLE             = "X-SCROLLABLE"
        self.WARMUP                 = "X-WARMUP"

        self.WIDTH                  = "X-WIDTH"
        self.HEIGHT                 = "X-HEIGHT"

        self.CUSTOM_EVENT_DATA      = "X-CUSTOM-EVENT-DATA"
        self.CUSTOM_EVENT_NAME      = "X-CUSTOM-EVENT-NAME"
        self.CUSTOM_EVENT_HTML_DATA = "X-CUSTOM-EVENT-HTML-DATA"

        
        self.LOCATION               = "Location"

    
XHEADERS = XHEADERS_ENUM()

# SAMPLE STUFF

MRAID_URI       = "http://ads.moceanads.com/ad?zone=%d" % 98468
MRAID_VIDEO_IS  = "http://admarvel.s3.amazonaws.com/demo/mraid/MRAID_v2_video_interstitial.txt"
MEDIATED_ADMOB  = "http://ads.mopub.com/m/ad?v=6&id=d06ebc5615c14dca880d141ea5989c83&nv=2.4.0&dn=unknown%2CAndroid%20SDK%20built%20for%20x86%2Csdk_x86&udid=sha%3A85a32f54c0e92908e6eb03c4f4521f89b88de2ae&z=%2B0400&o=l&sc_a=1.33125&mr=1&mcc=310&mnc=260&iso=us&cn=Android&ct=3&av=1.0&android_perms_ext_storage=0"

VAST_LINEAR_WITH_COMPANIONS = "http://ad3.liverail.com/?LR_PUBLISHER_ID=1331&LR_CAMPAIGN_ID=229&LR_SCHEMA=vast2"
#VAST_NON_LINEAR_WITH_LINEAR_TAKEOVER = "http://ad3.liverail.com/?LR_PUBLISHER_ID=1331&LR_CAMPAIGN_ID=227&LR_SCHEMA=vast2"
VAST_NON_LINEAR = "http://ad3.liverail.com/?LR_PUBLISHER_ID=1331&LR_CAMPAIGN_ID=228&LR_SCHEMA=vast2"
VAST_2_REGULAR_LINEAR = "http://demo.tremorvideo.com/proddev/vast/vast2RegularLinear.xml"
VAST_2_NON_LINEAR = "http://demo.tremorvideo.com/proddev/vast/vast2Nonlinear.xml"
VAST_1_NON_LINEAR = "http://demo.tremorvideo.com/proddev/vast/vast1Nonlinear.xml"

VAST_INLINE_NONLINEAR = "http://demo.tremorvideo.com/proddev/vast/vast_inline_nonlinear.xml"
"http://demo.tremorvideo.com/proddev/vast/vast_wrapper_linear_1.xml"

ADFOX_PARAMS = { "ad_url"       : "http://ads.adfox.ru/164487/getCode?p1=cggq&p2=ejnb&pfc=a&pfb=a"
               , "caching"      : True 
               , "data_timeout" : 5000
               , "use_flash"    : True 
               , "use_anim_gif" : True 
               , "dialog_yes"   : "Yaar" 
               }

ADMOB_PARAMS = { 'unit' : "ca-app-pub-7744316982922900/2373326072" }

