import urllib2
import json
from faan.core.model.general.geo import GeoCity, GeoCountry, GeoRegion
from sqlalchemy.sql.expression import or_
from faan.core.model.meta import Session
from faan.core.X.xflask import capp
from decimal import Decimal

GMAPS_URI = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=false&language=%s&key=AIzaSyAmtFkFoQtDwKCMahtWtew-5Frv914_UXc"

def geoQuery(lat, lng, language):
    res = { 'code'      : ''
          , 'country'   : ''
          , 'region'    : ''
          , 'city'      : ''
          }
    
    uri = GMAPS_URI % (lat, lng, language)
    data = json.loads(urllib2.urlopen(uri).read())
    
    if  data['status'] != "OK":
        print data
    
    if  data['status'] != "OK" or \
        not data['results']    or \
        len(data['results'][0]['address_components']) < 3:
        
        return None 
    
    for ac in data['results'][0]['address_components']:
        if ac['types'][0] == 'country':
            res['code'] = ac['short_name']
            res['country'] = ac['long_name']
            
        if ac['types'][0] == 'administrative_area_level_1':
            res['region'] = ac['long_name']
            
        if ac['types'][0] == 'locality':
            res['city'] = ac['long_name']
            
    return res



def geoForCoordsX(lat, lng):
    res = { 'en' : geoQuery(lat, lng, 'en')
          , 'ru' : geoQuery(lat, lng, 'ru') 
          }
    
    if not res['en'] or not res['ru']:
        return
    
    # Country
    
    country = GeoCountry.Single(GeoCountry.code == res['en']['code']) or GeoCountry.Default()
    country.name    = res['ru']['country']
    country.en_name = res['en']['country']
    country.code    = res['en']['code']
    
    Session.add(country)
    Session.flush()
    Session.commit()
    
    # Region
    
    if (not res['ru']['region']) and (not res['en']['region']):
        capp.logger.debug(">#>#>#> NOT REGION")
        return
    
    region = GeoRegion.Single(GeoRegion.country == country.id, 
                              or_(GeoRegion.name    == res['ru']['region'], 
                                  GeoRegion.en_name == res['en']['region'])) or GeoRegion.Default()
                                  
    region.country = country.id
    region.name    = region.name    or res['ru']['region']
    region.en_name = region.en_name or res['en']['region']
    
    Session.add(region)
    Session.flush()
    Session.commit()  

    # City
    
    if (not res['ru']['city']) or (not res['en']['city']):
        capp.logger.debug(" >#>#>#> NOT CITY")
        return
    
    city = GeoCity.Single(GeoCity.region == region.id, 
                              or_(GeoCity.name    == res['ru']['city'], 
                                  GeoCity.en_name == res['en']['city'])) or GeoCity.Default()
    
    city.region     = region.id
    city.name       = city.name or res['ru']['city']
    city.en_name    = city.en_name or res['en']['city']
    city.gm_en_code = '%s > %s > %s' % (res['en']['code'], res['en']['region'], res['en']['city'])
    city.gm_ru_code = '%s > %s > %s' % (res['ru']['code'], res['ru']['region'], res['ru']['city'])

    #city.longitude  = city.longitude or Decimal(0)
    #city.latitude  = city.latitude or Decimal(0) 

    Session.add(city)
    Session.flush()
    Session.commit()  
































