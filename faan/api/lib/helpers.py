# -*- coding: utf-8 -*-
import urllib
from urlparse import urlunparse

def url_append(url, params=None, params2=None):
    if params is None:
        params = []
    if params2 is None:
        params2 = []
    if type(params) == dict:
        params = params.items()
    if type(params2) == dict:
        params2 = params2.items()
    qs = urllib.urlencode(dict(params + params2))
    return urlunparse(['', '', url, '', qs, '', ])
