from faan.core.X.xflask import request
import urllib

def mobfoxRequest():
    uri = "http://my.mobfox.com/request.php"
    
    params = { 'rt'     : 'api'
             , 'r_type' : 'video'
             , 'r_resp' : 'vast20'
             , 's'      : '95841e24f172a6b092c4e24e07c76e5b'
             #, 'u'      : 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 4_0 like Mac OS X; en-us) AppleWebKit/532.9 (KHTML, like Gecko) Version/4.0.5 Mobile/8A293 Safari/6531.22.7' 
             , 'u'      : request.user_agent.string
             , 'i'      : request.remote_addr
             , 'v'      : '3.0'
             }
    
    if request.values['platform'] == 'ios':
        params['o_iosadvid']  = request.values['id_ios_idfv']
    else:
        params['o_androidid'] = request.values['id_android_id']
        
    for hn, hv in request.headers:
        if hn.lower() != "user-agent":
            params['h' + hn.lower()] = hv
        
    uri = uri + "?" + urllib.urlencode(params)
    
    
    f = urllib.urlopen(uri)
    #print f.geturl() # Prints the final URL with parameters.
    msg = f.read()
    
    if "<error>No Ad Available</error>" not in msg:
        print ">>>>>MOBFOX>>>>>>: [%s]" % msg
    