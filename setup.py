from setuptools import setup, find_packages

setup(
    name = 'faan.api',
    version = '0.3.8',
    description = '',
    author = '',
    author_email = '',
    url = '',
    namespace_packages = ['faan'],
    install_requires = [
        "Flask>=0.10",
        "SQLAlchemy>=0.7",
        "PasteDeploy",
    ],
    packages = find_packages(exclude = ['ez_setup']),
    include_package_data = True,
    test_suite = 'nose.collector',
    zip_safe = False,
    entry_points = """
    [paste.app_factory]
    main = faan.api.app:make_app
    """
)

