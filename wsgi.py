"""
Sample for local.py
"""

import os
from werkzeug.serving import run_simple
from paste.deploy import loadapp


application = loadapp('config:' + os.path.join(os.path.dirname(__file__), 'configurations/local.ini'))

def serve(port=8099):
    run_simple('localhost', port, application, use_reloader=False, use_debugger=True)
    
if __name__ == "__main__":
    serve()